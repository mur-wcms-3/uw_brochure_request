// This makes a group of checkboxes behave like radios, with the added
// functionality of being able to uncheck them all.
// The checkboxes must have the same classes.
jQuery(document).ready(function ($) {
  $("#edit-lead-type").change(function () {
    let entry_year = $("label[for=edit-entry-year]");
    console.log($(this).val());
    if($(this).val() == 'parent') {
      $(entry_year).html('When does your child plan to begin university? (required)');
    } else {
      $(entry_year).html('When do you plan to begin university? (required)');
    }
  });
});
