<?php

namespace Drupal\uw_brochure_request;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Brochure entities.
 *
 * @ingroup uw_brochure_request
 */
class BrochureEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritDoc}
   */
  public function load() {

    $entity_query = \Drupal::service('entity.query')->get('brochure_entity');
    $header = $this->buildHeader();

    $entity_query->pager(50);
    $entity_query->tableSort($header);

    $uids = $entity_query->execute();

    return $this->storage->loadMultiple($uids);
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = [
      'data' => $this->t('Brochure'),
      'field' => 'name',
      'specifier' => 'name',
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['code'] = [
      'data' => $this->t('CRM Code'),
      'field' => 'field_brochure_code',
      'specifier' => 'field_brochure_code',
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['interest'] = [
      'data' => $this->t('Interest'),
      'field' => 'field_brochure_interest',
      'specifier' => 'field_brochure_interest',
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['faculty'] = [
      'data' => $this->t('Is Faculty'),
      'field' => 'field_brochure_is_faculty',
      'specifier' => 'field_brochure_is_faculty',
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['order'] = [
      'data' => $this->t('Display Order'),
      'field' => 'field_brochure_order',
      'specifier' => 'field_brochure_order',
      'class' => [RESPONSIVE_PRIORITY_LOW],
      'sort' => 'asc',
    ];
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.brochure_entity.edit_form',
      ['brochure_entity' => $entity->id()]
    );
    $row['code'] = $entity->field_brochure_code->value;
    $row['interest'] = $entity->field_brochure_interest->value;
    $row['faculty'] = $entity->field_brochure_is_faculty->value == 1 ? "Yes" : "";
    $row['order'] = $entity->field_brochure_order->value;
    return $row + parent::buildRow($entity);
  }

}
