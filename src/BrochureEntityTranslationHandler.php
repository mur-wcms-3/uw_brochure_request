<?php

namespace Drupal\uw_brochure_request;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for brochure_entity.
 */
class BrochureEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
