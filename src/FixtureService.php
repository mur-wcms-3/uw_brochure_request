<?php
// phpcs:ignoreFile

namespace Drupal\uw_brochure_request;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Fixture Service.
 */
class FixtureService {

  /**
   * Messenger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Populates the default values for Brochure entities.
   */
  public function createDefaultBrochures() {
    // Load brochures array to process.
    $brochures = $this->getBrochures();

    $store = $this->entityTypeManager->getStorage('brochure_entity');

    // Loop through each of our brochure records.
    foreach ($brochures as [$code, $name, $interest, $order]) {
      // Check if the brochure already exists, as it needs to be unique.
      $exists = $this->entityTypeManager->getStorage('brochure_entity')->getQuery()
        ->condition('name', $name)
        ->execute();

      if (!empty($exists)) {

        $this->messenger->addWarning('"' . $name . '" already exists. Skipped.');

      }
      else {
        // Create the brochure entity.
        $store->create([
          'name' => $name,
          'field_brochure_code' => $code,
          'field_brochure_interest' => $interest,
          'field_brochure_label' => $name,
          'field_brochure_order' => $order,
          'field_brochure_print_availability' => TRUE,
          'field_brochure_is_faculty' => stripos($name, 'Faculty') !== FALSE,
        ],
        )->save();

        $this->messenger->addMessage('"' . $name . '" created.');
      }
    }
  }

  /**
   * Returns array of brochures.
   *
   * @return array[]
   *   Brochures.
   */
  private function getBrochures() {
    // phpcs:disable
    return [
      ["num_national_admissions_c", 'Canadian viewbook', '', 0],
      ["num_intl_admissions_c", 'International viewbook', '', 1],
      ["num_arts_c", 'Faculty of Arts', '^ART^', 10],
      ["num_engineering_c", 'Faculty of Engineering', '^ENG^', 10],
      ["num_environment_c", 'Faculty of Environment', '^ENV^', 10],
      ["num_ahs_c", 'Faculty of Health', '^AHS^', 10],
      ["num_math_c", 'Faculty of Mathematics', '^MAT^', 10],
      ["num_science_c", 'Faculty of Science', '^SCI^', 10],
      ["num_saf_c", 'Accounting and Finance', '^AFM^', 10],
      ["num_architecture_c", 'Architecture', '^ARCH^', 10],
      ["num_aviation_c", 'Aviation', '', 10],
      ["num_cs_c", 'Computer Science', '^CS^', 10],
      ["num_gbda_c", 'Global Business and Digital Arts', '^ART^', 10],
      ["num_bus_account_c", 'Mathematics, Business and Accounting', '^ART^,^BUS^,^MAT^', 10],
      ["num_career_health_c", 'Medicine and Health Care', '^AHS^,^SCI^', 10],
      ["num_optometry_c", 'Optometry', '^SCI^', 10],
      ["num_pharmacy_c", 'Pharmacy', '^SCI^', 10],
      ["num_grebel_c", 'Conrad Grebel University College', '^Grebel^', 20],
      ["num_renison_c", 'Renison University College', '^Renison^', 21],
      ["num_stj_c", 'St. Jerome\'s University', '^SJU^', 22],
      ["num_stp_c", 'St. Paul\'s University College', '^STP^', 23],
    ];
    // phpcs:enable
  }

}
