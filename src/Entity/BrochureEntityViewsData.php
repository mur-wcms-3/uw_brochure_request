<?php

namespace Drupal\uw_brochure_request\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Brochure entities.
 */
class BrochureEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
