<?php

namespace Drupal\uw_brochure_request\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Brochure entity.
 *
 * @ingroup uw_brochure_request
 *
 * @ContentEntityType(
 *   id = "brochure_entity",
 *   label = @Translation("Brochure"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\uw_brochure_request\BrochureEntityListBuilder",
 *     "views_data" = "Drupal\uw_brochure_request\Entity\BrochureEntityViewsData",
 *     "translation" = "Drupal\uw_brochure_request\BrochureEntityTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\uw_brochure_request\Form\BrochureEntityForm",
 *       "add" = "Drupal\uw_brochure_request\Form\BrochureEntityForm",
 *       "edit" = "Drupal\uw_brochure_request\Form\BrochureEntityForm",
 *       "delete" = "Drupal\uw_brochure_request\Form\BrochureEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\uw_brochure_request\BrochureEntityHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\uw_brochure_request\BrochureEntityAccessControlHandler",
 *   },
 *   base_table = "brochure_entity",
 *   data_table = "brochure_entity_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer brochure entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/mur/brochure_entity/{brochure_entity}",
 *     "add-form" = "/admin/mur/brochure_entity/add",
 *     "edit-form" = "/admin/mur/brochure_entity/{brochure_entity}/edit",
 *     "delete-form" = "/admin/mur/brochure_entity/{brochure_entity}/delete",
 *     "collection" = "/admin/mur/brochure_entity",
 *   },
 *   field_ui_base_route = "brochure_entity.settings"
 * )
 */
class BrochureEntity extends ContentEntityBase implements BrochureEntityInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Brochure entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Brochure is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
