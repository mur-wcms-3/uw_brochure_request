<?php

namespace Drupal\uw_brochure_request\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Brochure entities.
 *
 * @ingroup uw_brochure_request
 */
interface BrochureEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Brochure name.
   *
   * @return string
   *   Name of the Brochure.
   */
  public function getName();

  /**
   * Sets the Brochure name.
   *
   * @param string $name
   *   The Brochure name.
   *
   * @return \Drupal\uw_brochure_request\Entity\BrochureEntityInterface
   *   The called Brochure entity.
   */
  public function setName($name);

  /**
   * Gets the Brochure creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Brochure.
   */
  public function getCreatedTime();

  /**
   * Sets the Brochure creation timestamp.
   *
   * @param int $timestamp
   *   The Brochure creation timestamp.
   *
   * @return \Drupal\uw_brochure_request\Entity\BrochureEntityInterface
   *   The called Brochure entity.
   */
  public function setCreatedTime($timestamp);

}
