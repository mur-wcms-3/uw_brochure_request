<?php

namespace Drupal\uw_brochure_request;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Brochure entity.
 *
 * @see \Drupal\uw_brochure_request\Entity\BrochureEntity.
 */
class BrochureEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\uw_brochure_request\Entity\BrochureEntityInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished brochure entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published brochure entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit brochure entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete brochure entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add brochure entities');
  }

}
