<?php

namespace Drupal\uw_brochure_request\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Brochure entities.
 *
 * @ingroup uw_brochure_request
 */
class BrochureEntityDeleteForm extends ContentEntityDeleteForm {


}
