<?php

namespace Drupal\uw_brochure_request\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\State\State;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\uw_mur_base\HelperService;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\uw_mur_base\CRMService;

/**
 * Class BrochureRequestForm.
 */
class BrochureBulkRequestForm extends FormBase {

  /**
   * State.
   *
   * @var Drupal\Core\State\State
   */
  protected $state;

  /**
   * MUR helper service.
   *
   * @var Drupal\uw_mur_base\HelperService
   */
  protected $murHelper;

  /**
   * CRM service.
   *
   * @var Drupal\uw_mur_base\CRMService
   */
  protected $crm;

  /**
   * Messenger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('uw_mur_base.helper'),
      $container->get('uw_mur_base.crm'),
      $container->get('messenger'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    State $state,
    HelperService $murHelper,
    CRMService $crm,
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->state = $state;
    $this->murHelper = $murHelper;
    $this->crm = $crm;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'brochure_bulk_request_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $brochure_header = $this->state->get('uw_brochure_request.brochure_header_bulk');
    $form['header_content'] = [
      '#markup' => "<div>$brochure_header</div>",
    ];

    $max_brochures = $this->state->get('uw_brochure_request.max_brochures_bulk', 4);
    $form['max_brochures_label_bulk'] = [
      '#markup' => "<p>Please select upto $max_brochures brochures.</p>",
    ];

    $all_brochures_bulk = $this->state->get('uw_brochure_request.all_brochures_bulk', FALSE);
    $all_faculty_brochures_bulk = $this->state->get('uw_brochure_request.all_faculty_brochures_bulk', FALSE);
    $brochures = [
      '' => '- Select -',
    ];
    if ($all_brochures_bulk) {
      $brochures['Bundles']['all'] = 'All brochures';
    }
    if ($all_faculty_brochures_bulk) {
      $brochures['Bundles']['faculty'] = 'All faculty brochures';
    }

    $brochures['Brochures'] = $this->getBrochures();

    // Generate brochure fields up to max brochures allowed.
    for ($i = 1; $i <= $max_brochures; $i++) {
      $form["brochure_$i"] = [
        '#type' => 'select',
        '#title' => "Brochure #$i",
        '#options' => $brochures,
      ];
      $form["brochure_" . $i . "_amount"] = [
        '#type' => 'number',
        '#title' => "How many of brochure #$i would you like?",
        '#maxlength' => 5,
        '#size' => 5,
        '#min' => 0,
        '#default_value' => 0,
      ];
    }

    $form['spacer'] = [
      '#markup' => '<br><h3>Shipping details</h3>',
    ];

    $form['firstname'] = [
      '#type' => 'textfield',
      '#title' => 'First name (required)',
      '#required' => TRUE,
      '#size' => 25,
    ];

    $form['lastname'] = [
      '#type' => 'textfield',
      '#title' => 'Last name (required)',
      '#required' => TRUE,
      '#size' => 25,
    ];

    $form['email'] = [
      '#type' => 'email',
      '#pattern' => '[^@\s]+@[^@\s]+\.[^@\s]+',
      '#title' => 'Email address (required)',
      '#required' => TRUE,
      '#size' => 25,
    ];

    $form['school'] = [
      '#type' => 'textfield',
      '#title' => $this->t('School name (required)'),
      '#required' => TRUE,
      '#size' => 25,
    ];

    $form['address'] = [
      '#type' => 'textarea',
      '#title' => $this->t('School address (required)'),
      '#rows' => 2,
      '#required' => TRUE,
    ];

    $form['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City (required)'),
      '#size' => 25,
      '#required' => TRUE,
    ];

    $form['province'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Province/state (required)'),
      '#size' => 25,
      '#required' => TRUE,
    ];

    $form['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country (required)'),
      '#options' => $this->murHelper->getCountries(),
      '#default_value' => 'CAN',
      '#required' => TRUE,
    ];

    $form['postal_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Postal/zip code (required)'),
      '#size' => 25,
      '#required' => TRUE,
    ];

    $form['phone_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone number (required)'),
      '#size' => 25,
      '#required' => TRUE,
      '#attributes' => [
        'type' => 'tel',
        'placeholder' => '1 (555) 123-4567',
        'pattern' => '[0-9\-()\s\+]*',
      ],
    ];

    $newsletter_label = $this->state->get('uw_brochure_request.brochure_newsletter_label', '<strong>Newsletter Signup</strong>');
    $form['news_can'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Counsellor newsletter for Canadian counsellors'),
      '#prefix' => "<div>$newsletter_label</div>",
      '#return_value' => 'canadian_newsletter',
    ];
    $form['news_int'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Counsellor newsletter for international counsellors'),
      '#return_value' => 'international_newsletter',
    ];

    $opt_in_content = $this->state->get('uw_brochure_request.opt_in_content_bulk');
    $opt_in_label = $this->state->get('uw_brochure_request.opt_in_label_bulk', 'I agree');
    $form['opt_in'] = [
      '#type' => 'checkbox',
      '#title' => $opt_in_label,
      '#prefix' => "<div>$opt_in_content</div>",
      '#value' => TRUE,
      '#required' => FALSE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Request Brochure(s)'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $max_brochures = $this->state->get('uw_brochure_request.max_brochures_bulk', 4);
    $brochures = [];
    for ($i = 1; $i <= $max_brochures; $i++) {
      $brochure = $form_state->getValue("brochure_$i");
      $brochures[] = $brochure;
    }
    $brochures = array_filter($brochures, function ($v, $k) {
      return !empty($v);
    }, ARRAY_FILTER_USE_BOTH);
    if (count(array_unique($brochures)) < count($brochures)) {
      $form_state->setError($form, "You may only choose one of each brochure.");
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_state->disableRedirect(TRUE);

    $brochures = [];
    $max_brochures = $this->state->get('uw_brochure_request.max_brochures_bulk', 4);
    for ($i = 1; $i <= $max_brochures; $i++) {
      $brochure = $form_state->getValue("brochure_$i");
      $count = $form_state->getValue("brochure_" . $i . "_amount");

      if (empty($count) || empty($brochure)) {
        continue;
      }

      // If all or faculty option selected, add all the appropriate brochures.
      if ($brochure == 'all' || $brochure == 'faculty') {
        $is_faculty = $brochure == 'faculty';
        foreach ($this->getBrochures($is_faculty) as $key => $val) {
          if (array_key_exists($key, $brochures)) {
            $brochures[$key] += $count;
          }
          else {
            $brochures[$key] = $count;
          }
        }
      }
      else {
        if (array_key_exists($brochure, $brochures)) {
          $brochures[$brochure] += $count;
        }
        else {
          $brochures[$brochure] = $count;
        }
      }
    }

    $newsletter_types = "";
    if ($form_state->getValue('news_can')) {
      $newsletter_types .= "^canadian_newsletter^";
    }
    if ($form_state->getValue('news_int')) {
      $newsletter_types .= ",^international_newsletter^";
    }
    $newsletter_types = trim($newsletter_types, ',');

    // Build up the lead record to create.
    $contact = [
      "firstname" => $form_state->getValue('firstname'),
      "lastname" => $form_state->getValue('lastname'),
      "email" => $form_state->getValue('email'),
      "address" => $form_state->getValue('address'),
      "city" => $form_state->getValue('city'),
      "province" => $form_state->getValue('province'),
      "country" => $form_state->getValue('country'),
      "postal_code" => $form_state->getValue('postal_code'),
      "phone_number" => $form_state->getValue('phone_number'),
      "brochures" => $brochures,
      "newsletter_types" => $newsletter_types,
      "school" => $form_state->getValue('school'),
    ];

    // Create the contact.
    $response = $this->addContact($contact);
    $contact['id'] = $response;

    // Create the brochure request and attach to lead.
    $response = $this->addBrochureRequest($contact);

    // Redirect to the appropriate page as set in settings.
    $redirect_id = $this->state->get('uw_brochure_request.return_url_bulk');
    if (!empty($redirect_id)) {
      $form_state->setRedirect(
        'entity.node.canonical',
        ['node' => $redirect_id]
      );
    }
    else {
      $this->messenger
        ->addStatus("Your brochure request has been submitted.");
    }

  }

  /**
   * Retrieves the list of brochures to display on the form.
   *
   * @return array
   *   Brochures.
   */
  private function getBrochures($is_faculty = FALSE) {
    // Get all published brochures.
    $query = $this->entityTypeManager->getStorage('brochure_entity')->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('field_brochure_print_available', TRUE)
      ->sort('field_brochure_order', 'ASC')
      ->sort('name', 'ASC');

    if ($is_faculty) {
      $query->condition('field_brochure_is_faculty', TRUE);
    }

    $brochure_ids = $query->execute();
    $brochures = $this->entityTypeManager
      ->getStorage('brochure_entity')
      ->loadMultiple($brochure_ids);

    $options = [];
    foreach ($brochures as $brochure) {
      $options[$brochure->field_brochure_code->value] = $brochure->field_brochure_label->value;
    }

    return $options;
  }

  /**
   * Adds a contact to the CRM.
   *
   * $contact
   *   Contact.
   *
   * @return array
   *   Response returned.
   */
  private function addContact($contact) {
    $crm = $this->crm;

    $assigned_to_user_id = $this->state->get('uw_brochure_request.assigned_user_id');
    $campaign_name = $this->state->get('uw_brochure_request.campaign_name');
    $campaign_id = $this->state->get('uw_brochure_request.campaign_id');

    $session_id = $crm->getSession();

    $set_entry_parameters = [
      "session" => $session_id,
      "module_name" => "Contacts",
      "name_value_list" => [
        [
          "first_name" => $contact['firstname'],
          "last_name" => $contact['lastname'],
          "email1" => $contact['email'],
          "primary_address_street" => $contact['address'],
          "primary_address_city" => $contact['city'],
          "primary_address_state" => $contact['province'],
          "primary_address_country" => $contact['country'],
          "primary_address_postalcode" => $contact['postal_code'],
          "phone_home" => $contact['phone_number'],
          "campaign_name" => $campaign_name,
          "campaign_id" => $campaign_id,
          "assigned_user_id" => $assigned_to_user_id,
          "requested_newsletter_c" => $contact['newsletter_types'],
          "account_name" => $contact['school'],
        ],
      ],
    ];

    $response = $crm->call("set_entries", $set_entry_parameters);

    return $response['ids'][0];
  }

  /**
   * Creates a brochure request, attaches lead to the brochure record.
   *
   * $contact
   *   Contact.
   *
   * @return array
   *   Response.
   */
  private function addBrochureRequest($contact) {
    $crm = $this->crm;
    $session_id = $crm->getSession();

    $brochures = [
      "assigned_user_id" => "e0cc0dcd-1a4f-11e6-5760-559e886bf95d",
      "name" => $contact['firstname'] . " " . $contact['lastname'] . " - " . date("Y-m-d"),
      "letter_type" => "guidance",
      "fulfillment_status_c" => 'not_complete',
    ];
    // Add each brochure requested.
    foreach ($contact['brochures'] as $key => $value) {
      $brochures[$key] = $value;
    }

    $set_entry_parameters = [
      "session" => $session_id,
      "module_name" => "brs_request",
      "name_value_list" => [$brochures],
    ];

    // Send request to CRM.
    $response = $crm->call("set_entries", $set_entry_parameters);
    $brochure_id = $response["ids"][0];

    // Create relationship.
    $set_relationship_parameters = [
      "session" => $session_id,
      "module_name" => "Contacts",
      "module_id" => $contact['id'],
      "link_field_name" => "contacts_brs_request_1",
      "related_ids" => [$brochure_id],
      "name_value_list" => NULL,
      "delete" => 0,
    ];

    $response = $crm->call("set_relationship", $set_relationship_parameters);

    return $response;
  }

}
