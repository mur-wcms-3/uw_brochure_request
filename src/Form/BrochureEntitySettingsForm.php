<?php

namespace Drupal\uw_brochure_request\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\core\Url;

/**
 * Brochure Settings Form.
 *
 * @ingroup uw_brochure_request
 */
class BrochureEntitySettingsForm extends FormBase {
  /**
   * State.
   *
   * @var Drupal\Core\State\State
   */
  protected $state;

  /**
   * Fixture service.
   *
   * @var Drupal\uw_brochure_request\FixtureService
   */
  protected $fixture;

  /**
   * Messenger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->messenger = $container->get('messenger');
    $instance->fixture = $container->get('uw_brochure_request.fixtures');
    $instance->state = $container->get('state');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'brochureentity_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $return_url_digital = $form_state->getValue('return_url_digital');
    $this->state->set('uw_brochure_request.return_url_digital', $return_url_digital);

    $return_url_mailed = $form_state->getValue('return_url_mailed');
    $this->state->set('uw_brochure_request.return_url_mailed', $return_url_mailed);

    $campaign_name = $form_state->getValue('campaign_name');
    $this->state->set('uw_brochure_request.campaign_name', $campaign_name);

    $campaign_id = $form_state->getValue('campaign_id');
    $this->state->set('uw_brochure_request.campaign_id', $campaign_id);

    $assigned_user_id = $form_state->getValue('assigned_user_id');
    $this->state->set('uw_brochure_request.assigned_user_id', $assigned_user_id);

    $max_brochures = $form_state->getValue('max_brochures');
    $this->state->set('uw_brochure_request.max_brochures', $max_brochures);

    $brochure_header = $form_state->getValue('brochure_header');
    $this->state->set('uw_brochure_request.brochure_header', $brochure_header['value']);

    $opt_in_content = $form_state->getValue('opt_in_content');
    $this->state->set('uw_brochure_request.opt_in_content', $opt_in_content['value']);

    $opt_in_label = $form_state->getValue('opt_in_label');
    $this->state->set('uw_brochure_request.opt_in_label', $opt_in_label);

    // Bulk Settings.
    $return_url_bulk = $form_state->getValue('return_url_bulk');
    $this->state->set('uw_brochure_request.return_url_bulk', $return_url_bulk);

    $campaign_name_bulk = $form_state->getValue('campaign_name_bulk');
    $this->state->set('uw_brochure_request.campaign_name_bulk', $campaign_name_bulk);

    $campaign_id_bulk = $form_state->getValue('campaign_id_bulk');
    $this->state->set('uw_brochure_request.campaign_id_bulk', $campaign_id_bulk);

    $assigned_user_id_bulk = $form_state->getValue('assigned_user_id_bulk');
    $this->state->set('uw_brochure_request.assigned_user_id_bulk', $assigned_user_id_bulk);

    $max_brochures_bulk = $form_state->getValue('max_brochures_bulk');
    $this->state->set('uw_brochure_request.max_brochures_bulk', $max_brochures_bulk);

    $all_brochures_bulk = $form_state->getValue('all_brochures_bulk');
    $this->state->set('uw_brochure_request.all_brochures_bulk', $all_brochures_bulk);

    $all_faculty_brochures_bulk = $form_state->getValue('all_faculty_brochures_bulk');
    $this->state->set('uw_brochure_request.all_faculty_brochures_bulk', $all_faculty_brochures_bulk);

    $brochure_header_bulk = $form_state->getValue('brochure_header_bulk');
    $this->state->set('uw_brochure_request.brochure_header_bulk', $brochure_header_bulk['value']);

    $newsletter_label = $form_state->getValue('brochure_newsletter_label');
    $this->state->set('uw_brochure_request.brochure_newsletter_label', $newsletter_label['value']);

    $opt_in_content_bulk = $form_state->getValue('opt_in_content_bulk');
    $this->state->set('uw_brochure_request.opt_in_content_bulk', $opt_in_content_bulk['value']);

    $opt_in_label_bulk = $form_state->getValue('opt_in_label_bulk');
    $this->state->set('uw_brochure_request.opt_in_label_bulk', $opt_in_label_bulk);
  }

  /**
   * Defines the settings form for Brochure entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Vertical tabs.
    $form['tabs'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-tuition',
    ];

    $form['standard'] = [
      '#type' => 'details',
      '#title' => $this->t('Standard Request'),
      '#group' => 'tabs',
    ];

    $form['bulk'] = [
      '#type' => 'details',
      '#title' => $this->t('Bulk Request'),
      '#group' => 'tabs',
    ];

    $form['init'] = [
      '#type' => 'details',
      '#title' => $this->t('Initialization'),
      '#group' => 'tabs',
    ];

    $return_url_digital_id = $this->state
      ->get('uw_brochure_request.return_url_digital');
    if ($return_url_digital_id) {
      $return_url_digital_node = $this->entityTypeManager
        ->getStorage('node')
        ->load($return_url_digital_id);
    }
    else {
      $return_url_digital_node = NULL;
    }
    $form['standard']['return_url_digital'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#selection_settings' => [
        'target_bundles' => ['uw_ct_web_page'],
      ],
      '#default_value' => $return_url_digital_node ?: '',
      '#title' => $this->t('Return URL - Digital Brochures'),
      '#description' => $this->t('URL to return to after a <b>digital</b> brochure has been requested.'),
    ];

    $return_url_mailed_id = $this->state
      ->get('uw_brochure_request.return_url_mailed');
    if ($return_url_mailed_id) {
      $return_url_mailed_node = $this->entityTypeManager
        ->getStorage('node')
        ->load($return_url_mailed_id);
    }
    else {
      $return_url_mailed_node = NULL;
    }
    $form['standard']['return_url_mailed'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#selection_settings' => [
        'target_bundles' => ['uw_ct_web_page'],
      ],
      '#default_value' => $return_url_mailed_node ?: '',
      '#title' => $this->t('Return URL - Mailed Brochures'),
      '#description' => $this->t('URL to return to after a <b>mailed</b> brochure has been requested.'),
    ];

    $form['standard']['spacer'] = [
      '#markup' => '<br><br>',
    ];

    $campaign_name = $this->state
      ->get('uw_brochure_request.campaign_name', 'Brochure Request');
    $form['standard']['campaign_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CRM Campaign Name'),
      '#description' => $this->t('Campaign name to set as source campaign for new leads'),
      '#default_value' => $campaign_name,
    ];

    $campaign_id = $this->state
      ->get('uw_brochure_request.campaign_id', 'e0613ce9-662b-98f1-dcd2-5638e33e9a9a');
    $form['standard']['campaign_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CRM Campaign ID'),
      '#description' => $this->t('Campaign id to set for new leads.'),
      '#default_value' => $campaign_id,
    ];

    $assigned_user_id = $this->state
      ->get('uw_brochure_request.assigned_user_id', '34945510-3e74-84be-cbc5-5368edaefda4');
    $form['standard']['assigned_user_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CRM Assigned User ID'),
      '#description' => $this->t('User id to set new leads to.'),
      '#default_value' => $assigned_user_id,
    ];

    $form['standard']['spacer2'] = [
      '#markup' => '<br><br>',
    ];

    $max_brochures = $this->state
      ->get('uw_brochure_request.max_brochures', 4);
    $form['standard']['max_brochures'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum brochures allowed to be mailed.'),
      '#description' => $this->t('Max number of brochures that can be selected for mailed delivery. <br>Set to zero(0) to disable limit.'),
      '#default_value' => $max_brochures,
    ];

    $form['standard']['spacer3'] = [
      '#markup' => '<br><br>',
    ];

    $brochure_header = $this->state
      ->get('uw_brochure_request.brochure_header');
    $form['standard']['brochure_header'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Brochure Header Content'),
      '#description' => $this->t('Content to display just above the brochure selection.'),
      '#default_value' => $brochure_header,
    ];

    $opt_in_content = $this->state
      ->get('uw_brochure_request.opt_in_content');
    $form['standard']['opt_in_content'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Opt-in Label Content'),
      '#description' => $this->t('Content to display above the opt-in checkbox. Use for an opt-in disclaimer.'),
      '#default_value' => $opt_in_content,
    ];

    $opt_in_label = $this->state
      ->get('uw_brochure_request.opt_in_label', 'I agree');
    $form['standard']['opt_in_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Opt-in Label'),
      '#description' => $this->t('Content to display right beside the checkbox'),
      '#default_value' => $opt_in_label,
    ];

    // Bulk Request Settings.
    $return_url_bulk_id = $this->state
      ->get('uw_brochure_request.return_url_bulk');
    if ($return_url_bulk_id) {
      $return_url_bulk_node = $this->entityTypeManager
        ->getStorage('node')
        ->load($return_url_bulk_id);
    }
    else {
      $return_url_bulk_node = NULL;
    }
    $form['bulk']['return_url_bulk'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#selection_settings' => [
        'target_bundles' => ['uw_ct_web_page'],
      ],
      '#default_value' => $return_url_bulk_node ?: '',
      '#title' => $this->t('Return URL'),
      '#description' => $this->t('URL to return to after a <b>bulk</b> brochure request has been made.'),
    ];

    $form['bulk']['spacer'] = [
      '#markup' => '<br><br>',
    ];

    $campaign_name_bulk = $this->state
      ->get('uw_brochure_request.campaign_name_bulk', 'Brochure Request');
    $form['bulk']['campaign_name_bulk'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CRM Campaign Name'),
      '#description' => $this->t('Campaign name to set as source campaign for new leads'),
      '#default_value' => $campaign_name_bulk,
    ];

    $campaign_id_bulk = $this->state
      ->get('uw_brochure_request.campaign_id_bulk', 'e0613ce9-662b-98f1-dcd2-5638e33e9a9a');
    $form['bulk']['campaign_id_bulk'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CRM Campaign ID'),
      '#description' => $this->t('Campaign id to set for new leads.'),
      '#default_value' => $campaign_id_bulk,
    ];

    $assigned_user_id_bulk = $this->state
      ->get('uw_brochure_request.assigned_user_id_bulk', '34945510-3e74-84be-cbc5-5368edaefda4');
    $form['bulk']['assigned_user_id_bulk'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CRM Assigned User ID'),
      '#description' => $this->t('User id to set new leads to.'),
      '#default_value' => $assigned_user_id_bulk,
    ];

    $form['bulk']['spacer2'] = [
      '#markup' => '<br><br>',
    ];

    $max_brochures_bulk = $this->state
      ->get('uw_brochure_request.max_brochures_bulk', 4);
    $form['bulk']['max_brochures_bulk'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum brochures allowed to be ordered.'),
      '#description' => $this->t('Max number of brochures that can be selected for bulk delivery.'),
      '#default_value' => $max_brochures_bulk,
    ];

    $all_brochures_bulk = $this->state
      ->get('uw_brochure_request.all_brochures_bulk');
    $form['bulk']['all_brochures_bulk'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow 'All brochures' option."),
      '#description' => $this->t("Adds an 'All brochures' option to the selection options."),
      '#default_value' => $all_brochures_bulk,
      '#return_value' => TRUE,
    ];

    $all_faculty_brochures_bulk = $this->state
      ->get('uw_brochure_request.all_faculty_brochures_bulk');
    $form['bulk']['all_faculty_brochures_bulk'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Allow 'Faculty brochures only' option."),
      '#description' => $this->t("Adds an 'Faculty brochures only' option to the selection options."),
      '#default_value' => $all_faculty_brochures_bulk,
      '#return_value' => TRUE,
    ];

    $form['bulk']['spacer3'] = [
      '#markup' => '<br><br>',
    ];

    $brochure_header_bulk = $this->state
      ->get('uw_brochure_request.brochure_header_bulk');
    $form['bulk']['brochure_header_bulk'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Brochure Header Content'),
      '#description' => $this->t('Content to display just above the brochure selection.'),
      '#default_value' => $brochure_header_bulk,
    ];

    $newsletter_label = $this->state
      ->get('uw_brochure_request.brochure_newsletter_label');
    $form['bulk']['brochure_newsletter_label'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Newsletter Label Content'),
      '#description' => $this->t('Content to display above the newsletter checkboxes.'),
      '#default_value' => $newsletter_label,
    ];

    $opt_in_content_bulk = $this->state
      ->get('uw_brochure_request.opt_in_content_bulk');
    $form['bulk']['opt_in_content_bulk'] = [
      '#type' => 'text_format',
      '#format' => 'uw_tf_standard',
      '#title' => $this->t('Opt-in Label Content'),
      '#description' => $this->t('Content to display above the opt-in checkbox. Use for an opt-in disclaimer.'),
      '#default_value' => $opt_in_content_bulk,
    ];

    $opt_in_label_bulk = $this->state
      ->get('uw_brochure_request.opt_in_label_bulk', 'I agree');
    $form['bulk']['opt_in_label_bulk'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Opt-in Label'),
      '#description' => $this->t('Content to display right beside the checkbox'),
      '#default_value' => $opt_in_label_bulk,
    ];

    // Initialization Settings.
    $form['init']['defaults_label'] = [
      '#markup' => '<h3>Create Default Brochures</h3><p>Use this to initialize the default brochure list</p>',
    ];
    $form['init']['create_defaults'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create Default Brochures'),
      '#submit' => ['::createDefaults'],
    ];

    $form['init']['delete_entities'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete ALL Brochures'),
      '#submit' => ['::deleteEntities'],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Settings'),
      '#prefix' => '<br>',
    ];

    return $form;
  }

  /**
   * Calls the custom fixture service and populates entities.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function createDefaults(array &$form, FormStateInterface $form_state) {
    // Get the custom fixture service.
    $fixture_service = $this->fixture;
    // Call method to create the brochures.
    $fixture_service->createDefaultBrochures();
    // Display a message on completion.
    $this->messenger->addStatus($this->t('Default brochures have been populated.'));
  }

  /**
   * Redirects to delete entities page.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function deleteEntities(array &$form, FormStateInterface $form_state) {
    // Redirect to entity uninstall route.
    $url = Url::fromRoute('system.prepare_modules_entity_uninstall', ['entity_type_id' => 'brochure_entity']);
    $form_state->setRedirectUrl($url);
  }

}
