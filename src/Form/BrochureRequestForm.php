<?php

namespace Drupal\uw_brochure_request\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\State\State;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\uw_mur_base\HelperService;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\uw_mur_base\CRMService;

/**
 * Class Brochure Request Form.
 */
class BrochureRequestForm extends FormBase {

  /**
   * State.
   *
   * @var Drupal\Core\State\State
   */
  protected $state;

  /**
   * MUR helper service.
   *
   * @var Drupal\uw_mur_base\HelperService
   */
  protected $murHelper;

  /**
   * CRM service.
   *
   * @var Drupal\uw_mur_base\CRMService
   */
  protected $crm;

  /**
   * Messenger service.
   *
   * @var Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Entity type manager.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('uw_mur_base.helper'),
      $container->get('uw_mur_base.crm'),
      $container->get('messenger'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    State $state,
    HelperService $murHelper,
    CRMService $crm,
    MessengerInterface $messenger,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->state = $state;
    $this->murHelper = $murHelper;
    $this->crm = $crm;
    $this->messenger = $messenger;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'brochure_request_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $helper = $this->murHelper;

    $delivery = $form_state->getValue('delivery', 'digital');

    $form['#attached']['library'] = [
      'uw_brochure_request/brochure_request',
    ];

    $form['delivery'] = [
      '#type' => 'radios',
      '#title' => $this
        ->t('How would you like to receive your brochures?'),
      '#default_value' => 'digital',
      '#options' => [
        'digital' => $this->t('I’ll download digital brochures'),
        'mailed' => $this->t('I’d like the brochures mailed to me'),
      ],
      '#required' => TRUE,
      '#ajax' => [
        'event' => 'change',
        'callback' => "::getBrochureList",
        'wrapper' => 'brochure-container',
      ],
    ];

    $brochure_header = $this->state->get('uw_brochure_request.brochure_header');
    $form['header_content'] = [
      '#markup' => "<div>$brochure_header</div>",
    ];

    $form['brochure_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'brochure-container',
      ],
    ];

    $max_brochures = $this->state->get('uw_brochure_request.max_brochures', 4);
    $form['brochure_container']['brochures'] = [
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#options' => $this->getBrochures($delivery),
      '#required' => TRUE,
      '#title' => "Mailed brochure packages are limited to a maximum of $max_brochures brochures.<br>Please select upto $max_brochures brochures.",
      '#title_display' => 'before',
    ];

    $form['firstname'] = [
      '#type' => 'textfield',
      '#title' => 'First name (required)',
      '#required' => TRUE,
      '#size' => 25,
    ];

    $form['lastname'] = [
      '#type' => 'textfield',
      '#title' => 'Last name (required)',
      '#required' => TRUE,
      '#size' => 25,
    ];

    $form['email'] = [
      '#type' => 'email',
      '#pattern' => '[^@\s]+@[^@\s]+\.[^@\s]+',
      '#title' => 'Email address (required)',
      '#required' => TRUE,
      '#size' => 25,
    ];

    $form['lead_type'] = [
      '#type' => 'select',
      '#title' => $this->t('I am a... (required)'),
      '#required' => TRUE,
      '#options' => [
        "" => $this->t('- Select -'),
        "student" => $this->t('Student'),
        "parent" => $this->t('Parent/guardian'),
        "counsellor" => $this->t('Counsellor/teacher'),
        "other" => $this->t('Other'),
      ],
    ];

    $form['entry_year'] = [
      '#type' => 'select',
      '#title' => $this->t('When do you plan to begin university? (required)'),
      '#options' => $helper->getEntryYears(),
      '#required' => TRUE,
    ];

    $form['address'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mailing Address (required)'),
      '#rows' => 2,
      '#states' => [
        'visible' => [':input[name="delivery"]' => ['value' => 'mailed']],
        'required' => [':input[name="delivery"]' => ['value' => 'mailed']],
      ],
    ];

    $form['city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City (required)'),
      '#size' => 25,
      '#states' => [
        'visible' => [':input[name="delivery"]' => ['value' => 'mailed']],
        'required' => [':input[name="delivery"]' => ['value' => 'mailed']],
      ],
    ];

    $form['province'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Province/State (required)'),
      '#size' => 25,
      '#states' => [
        'visible' => [':input[name="delivery"]' => ['value' => 'mailed']],
        'required' => [':input[name="delivery"]' => ['value' => 'mailed']],
      ],
    ];

    $form['country'] = [
      '#type' => 'select',
      '#title' => $this->t('Country (required)'),
      '#options' => $helper->getCountries(),
      '#default_value' => 'CAN',
      '#states' => [
        'visible' => [':input[name="delivery"]' => ['value' => 'mailed']],
        'required' => [':input[name="delivery"]' => ['value' => 'mailed']],
      ],
    ];

    $form['postal_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Postal/zip code (required)'),
      '#size' => 25,
      '#states' => [
        'visible' => [':input[name="delivery"]' => ['value' => 'mailed']],
        'required' => [':input[name="delivery"]' => ['value' => 'mailed']],
      ],
    ];

    $form['phone_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone Number (required)'),
      '#size' => 25,
      '#attributes' => [
        'type' => 'tel',
        'placeholder' => '1 (555) 123-4567',
        'pattern' => '[0-9\-()\s\+]*',
      ],
      '#states' => [
        'visible' => [':input[name="delivery"]' => ['value' => 'mailed']],
        'required' => [':input[name="delivery"]' => ['value' => 'mailed']],
      ],
    ];

    $opt_in_content = $this->state->get('uw_brochure_request.opt_in_content');
    $opt_in_label = $this->state->get('uw_brochure_request.opt_in_label', 'Opt-in');
    $form['opt_in'] = [
      '#type' => 'checkbox',
      '#title' => $opt_in_label,
      '#prefix' => "<div>$opt_in_content</div>",
      '#value' => TRUE,
      '#required' => FALSE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Request Brochure(s)'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Limit the number of selected brochures allowed for mailed delivery.
    // Get fields and filter brochures down to just the selected keys.
    $delivery = $form_state->getValue('delivery');
    $brochures = $form_state->getValue('brochures');
    $brochure_ids = array_filter($brochures, function ($v, $k) {
      return $v != 0;
    }, ARRAY_FILTER_USE_BOTH);

    $max_brochures = $this->state->get('uw_brochure_request.max_brochures', 4);

    // Limit condition.
    if ($delivery == 'mailed' && count($brochure_ids) > $max_brochures && $max_brochures != 0) {
      $form_state->setErrorByName('brochures', "A maximum of $max_brochures brochures may be selected.  Please update your selection.");
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Differentiate between AJAX update and form submission.
    $trigger = (string) $form_state->getTriggeringElement()['#value'];

    if ($trigger == 'Request Brochure(s)') {

      $brochures = $form_state->getValue('brochures');
      $brochure_ids = array_filter($brochures, function ($v, $k) {
        return $v != 0;
      }, ARRAY_FILTER_USE_BOTH);
      $brochures = $this->entityTypeManager
        ->getStorage('brochure_entity')
        ->loadMultiple($brochure_ids);
      $options = [];
      foreach ($brochures as $brochure) {
        $options[$brochure->field_brochure_code->value] = $brochure->field_brochure_interest->value;
      }

      // Generate the faculty of interest string for CRM leads.
      $interests = [];
      foreach ($options as $interest) {
        $sub_interests = explode(',', $interest);
        if (is_array($sub_interests)) {
          foreach ($sub_interests as $int) {
            $interests[] = $int;
          }
        }
        else {
          $interests[] = $interest;
        }
      }
      $interests = array_filter(array_unique(array_values($interests)));
      asort($interests);
      $interests = implode(',', $interests);

      // Build up the lead record to create.
      $lead = [
        "firstname" => $form_state->getValue('firstname'),
        "lastname" => $form_state->getValue('lastname'),
        "email" => $form_state->getValue('email'),
        "lead_type" => $form_state->getValue('lead_type'),
        "entry_year" => $form_state->getValue('entry_year'),
        "address" => $form_state->getValue('address'),
        "city" => $form_state->getValue('city'),
        "province" => $form_state->getValue('province'),
        "country" => $form_state->getValue('country'),
        "postal_code" => $form_state->getValue('postal_code'),
        "phone_number" => $form_state->getValue('phone_number'),
        "opt_out" => '0',
        "lead_interests" => $interests,
        "brochures" => $options,
        "delivery" => $form_state->getValue('delivery'),
      ];

      // Create the lead.
      $response = $this->addLead($lead);
      $lead['id'] = $response;

      // Create the brochure request and attach to lead.
      $response = $this->addBrochureRequest($lead);

      // Redirect to the appropriate page as set in settings.
      if ($form_state->getValue('delivery') == 'mailed') {
        $redirect_id = $this->state
          ->get('uw_brochure_request.return_url_mailed');
      }
      else {
        $redirect_id = $this->state
          ->get('uw_brochure_request.return_url_digital');
      }

      if (!empty($redirect_id)) {
        $form_state->setRedirect(
          'entity.node.canonical',
          ['node' => $redirect_id]
        );
      }
      else {
        $this->messenger
          ->addStatus("Your brochure request has been submitted.");
      }
    }
    else {
      $form_state->setRebuild();
    }
  }

  /**
   * Retrieves the list of brochures to display on the form.
   *
   * @var string $type
   *   Options are digital or mailed.
   *
   * @return array
   *   Brochures array.
   */
  private function getBrochures($type = 'digital') {
    // Get all published brochures.
    $query = $this->entityTypeManager->getStorage('brochure_entity')->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->sort('field_brochure_order', 'ASC')
      ->sort('name', 'ASC');
    if ($type == 'mailed') {
      $query->condition('field_brochure_print_available', TRUE);
    }
    $brochure_ids = $query->execute();

    $brochures = $this->entityTypeManager
      ->getStorage('brochure_entity')
      ->loadMultiple($brochure_ids);

    $options = [];
    foreach ($brochures as $brochure) {
      $options[$brochure->id()] = $brochure->field_brochure_label->value;
    }

    return $options;
  }

  /**
   * Adds leads to the CRM.
   *
   * $lead
   *   Lead.
   *
   * @return array
   *   Response.
   */
  private function addLead($lead) {
    $crm = $this->crm;

    $assigned_to_user_id = $this->state->get('uw_brochure_request.assigned_user_id');
    $campaign_name = $this->state->get('uw_brochure_request.campaign_name');
    $campaign_id = $this->state->get('uw_brochure_request.campaign_id');

    $session_id = $crm->getSession();

    $set_entry_parameters = [
      "session" => $session_id,
      "module_name" => "Leads",
      "name_value_list" => [
        [
          "first_name" => $lead['firstname'],
          "last_name" => $lead['lastname'],
          "primary_address_street" => $lead['address'],
          "primary_address_city" => $lead['city'],
          "primary_address_state" => $lead['province'],
          "primary_address_country" => $lead['country'],
          "primary_address_postalcode" => $lead['postal_code'],
          "email1" => $lead['email'],
          "phone_home" => $lead['phone_number'],
          "admitterm_c" => $lead['entry_year'],
          "campaign_name" => $campaign_name,
          "campaign_id" => $campaign_id,
          "assigned_user_id" => $assigned_to_user_id,
          "status" => "1prospect",
          "email_opt_out" => $lead['opt_out'],
          "lead_type_c" => $lead['lead_type'],
          "faculty_c" => $lead['lead_interests'],
        ],
      ],
    ];

    $response = $crm->call("set_entries", $set_entry_parameters);

    return $response['ids'][0];
  }

  /**
   * Creates a brochure request, attaches lead to the brochure record.
   *
   * @return array
   *   Response.
   */
  private function addBrochureRequest($lead) {
    $crm = $this->crm;
    $session_id = $crm->getSession();

    // Fulfillment status depends on delivery method.
    $fulfillment_status = $lead['delivery'] == 'digital' ? 'digital' : 'not_complete';

    $brochures = [
      "assigned_user_id" => "e0cc0dcd-1a4f-11e6-5760-559e886bf95d",
      "name" => $lead['firstname'] . " " . $lead['lastname'] . " - " . date("Y-m-d"),
      "letter_type" => "grade12",
      "fulfillment_status_c" => $fulfillment_status,

    ];
    // Add each brochure requested.
    foreach ($lead['brochures'] as $key => $value) {
      $brochures[$key] = 1;
    }

    $set_entry_parameters = [
      "session" => $session_id,
      "module_name" => "brs_request",
      "name_value_list" => [$brochures],
    ];

    // Send request to CRM.
    $response = $crm->call("set_entries", $set_entry_parameters);
    $brochure_id = $response["ids"][0];

    // Create relationship.
    $set_relationship_parameters = [
      "session" => $session_id,
      "module_name" => "Leads",
      "module_id" => $lead['id'],
      "link_field_name" => "leads_brs_request_1",
      "related_ids" => [$brochure_id],
      "name_value_list" => NULL,
      "delete" => 0,
    ];

    $response = $crm->call("set_relationship", $set_relationship_parameters);

    return $response;
  }

  /**
   * AJAX update handler for list of brochures.
   *
   * @return mixed
   *   Brochures array.
   */
  public function getBrochureList($form, &$form_state) {
    return $form['brochure_container'];
  }

}
